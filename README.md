# Man(a)ga

A punny Manga Manager which bookmarks the manga chapter and page you have read for a specific manga on a mangasite.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for using Man(a)ga.

### Prerequisites

What things you need to install the software and how to install them
- Python 2.7
- Chromedriver
- Manga sites you know about

### Using

Download ChromeDriver from [Chromedriver Downloads](https://sites.google.com/a/chromium.org/chromedriver/downloads) and save it in your $PATH

```
mv <download_dir>/chromedriver /usr/bin or /usr/local/bin
```

Download the data.csv and Managa.py file from this repository

Run the Managa.py script

```
python Managa.py
```

The script will prompt the user on which
1. mangasite they want to use
2. manga to read

Once the suggestions have been selected by typing in the related number attached to each suggestion,
t he script will read from the data.csv file and prompt the user on which manga they want to read.
Once selected, a Selenium Chrome browser will pop up with an adblock extension and will
HTTP GET the manga the user wants to read


### Adding Manga to the data.csv

In order to add Manga to the dataset, simply copy and paste the url like so:
```
url = "http://some_manga_domain.com/thing/manga_title/chapter/page.html
```
as a csv row of:
```
http://some_manga_domain.com, thing/manga_title, chapter, page.html
```
This follows the convention of:
```
domain, anything_in_between_domain_and_chapter, chapter, page_including_extensions
```

TODO: need to add an "add url" feature to the script that automatically does the above.

### Exiting the script/application

In order to safely close the application either do a CTRL+C in the console
or close the Selenium Chrome Browser application, or close the Selenium Chrome Browser window

Be careful that any rogue Chromedriver processes don't hog your cpu overtime as 
sometimes they do not die properly. Check your running processes greping for
'chromedriver' and kill 'em.

### Known Issues

As this was a ~4 hour effort, bugs exist. The known bugs are:
- picking a suggestion out of index bounds throws an exception
- chromedriver processes not dying

### Decisions

Yes i know a csv is dumb for storing data, but i wanted this to be lightweight and not require a DB to configure
Yes i know the python sucks, i just did it as fast as i could and it kind of works (until it breaks)

## Built With

* [Python](http://www.python.org)
* [Python Selenium](http://selenium-python.readthedocs.io)


## Authors

* [**Daniel Pekevski**](https://gitlab.com/dpekevski) - *Initial work*

