from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchWindowException
import csv
import os
import logging
import sys

#constants
SEPARATOR = "/"
DATA_FILE = 'data.csv'
TEMP_FILE = 'tmp.csv'
ADBLOCK_CRX = 'AdBlock_v3.20.0.crx'

def myPrint(string):
	print "[MANGER.PY]\t" + string

# Generates suggestions based on the data in the data.csv file
# Can handle a suggestion for which manga site to pick
# Can handle a suggestion for which manga to pick
def generateChosenSuggestion(column, splitIndex, splitDelimiter, message, my_manga_domain = None):
	with open(DATA_FILE) as csvfile:
		reader = csv.reader(csvfile, delimiter=',')
		list = []
		for row in reader:
			if my_manga_domain is None:
				if row[0] not in list:
					list.append(row[column])
			elif my_manga_domain is not None:
				if row[0] == my_manga_domain:
					list.append(row[column])

	suggestion = ""
	index = 0
	for item in list:
		suggestion = suggestion + "\t\t\t" + str(index) + " " + item.split(splitDelimiter)[splitIndex] + "\n"
		index = index + 1

	result = raw_input("[MANGER.PY]\t" + message + " \n" + suggestion)

	return list[int(result)]

# Generates the full Manga url based on the suggestions chosen.
def generateFullMangaUrl(domain, manga, separator):
	index = 0
	with open(DATA_FILE) as csvfile:
		reader = csv.reader(csvfile, delimiter=',')
		for row in reader:
			if row[0] == domain and row[1] == manga:
				return (index, domain + separator + manga + separator + row[2] + separator + row[3])
			else:
				index = index + 1


my_manga_domain = generateChosenSuggestion(0, 1, "." ,"Please choose a mangasite you have used in the past:")
my_manga = generateChosenSuggestion(1, -1, "/","Please choose a manga from that site:", my_manga_domain)
index, my_full_url = generateFullMangaUrl(my_manga_domain, my_manga, SEPARATOR)

current = my_full_url

chop = webdriver.ChromeOptions()
chop.add_extension(ADBLOCK_CRX)
driver = webdriver.Chrome(chrome_options = chop)

myPrint("Loading page...")

driver.get(my_full_url)

myPrint("Check for an open Google Chrome browser window! (there should be one floating around)")
myPrint("Starting on Page: [" + current + "]")

while True:
	try:
		# when user has relocated to next page or chapter (change in url)
		if driver.current_url != current:

			# get the new chapter and page information
			current = driver.current_url
			url_data = current.split("/")
			new_url_chapter = url_data[-2]
			new_url_page = url_data[-1]

			with open(TEMP_FILE, 'wb') as f: # tmp output csv file for writing
				writer = csv.writer(f)
				with open(DATA_FILE,'r') as csvfile: # input csv file for reading
					reader = csv.reader(csvfile, delimiter=',')
					edit_index = 0

					for row in reader:  
						if index == edit_index:
							writer.writerow([my_manga_domain, my_manga, new_url_chapter, new_url_page])
						else:
							writer.writerow(row)
						edit_index = edit_index + 1

			# replace the old data file with the new tmp file (holding the up to date chapter and page)
			try:
				os.rename(TEMP_FILE, DATA_FILE)
			except WindowsError:
				os.remove(DATA_FILE)
				os.rename(TEMP_FILE, DATA_FILE)

			myPrint("Read Page: [" + driver.current_url + "]")

	except (KeyboardInterrupt, NoSuchWindowException) as e:
		print "\nBye"
		driver.quit()
		sys.exit()


